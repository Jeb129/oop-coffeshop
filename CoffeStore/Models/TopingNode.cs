﻿using CoffeStore.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrinkMenu.Models
{
    public class TopingNode : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public TopingNode(Toping tp)
        {
            Toping = tp;
            Count = 1;
        }
        public Toping Toping { get; set; }

        int _Count;
        public int Count
        {
            get { return _Count; }
            set
            {
                if (_Count != value)
                {
                    _Count = value;
                    OnPropertyChanged(nameof(Count));
                }
            }
        }
    }
}
