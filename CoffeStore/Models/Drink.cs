﻿using System.Collections.ObjectModel;
using System.Windows.Media;

namespace DrinkMenu.Models
{
    public class Drink : ProductBase
    {
        public Drink(string name, int cost,Color color, string avatarPath,int id = 0) : base(name,cost,color,id)
        {
            _AvatarPath = avatarPath;
            AllowedTopings = new()
            {};
        }

        public ObservableCollection<int> AllowedTopings { get; set; }
        private string _AvatarPath;
        public string AvatarPath
        {
            get { return _AvatarPath; }
            set
            {
                if (_AvatarPath != value)
                {
                    _AvatarPath = value;
                    OnPropertyChanged(nameof(AvatarPath));
                }
            }
        }
    }
}
