﻿using System;
using System.ComponentModel;
using System.Windows.Media;

namespace DrinkMenu.Models
{
    public abstract class ProductBase: INotifyPropertyChanged
    {
        public int ID { get; }
        private string _Name;
        public string Name
        {
            get { return _Name; }
            set
            {
                if (_Name != value)
                {
                    _Name = value;
                    OnPropertyChanged(nameof(Name));
                }
            }
        }
        private int _Cost;
        public int Cost
        {
            get { return _Cost; }
            set
            {
                if (_Cost != value)
                {
                    _Cost = value;
                    OnPropertyChanged(nameof(Cost));
                }
            }
        }
        private Color _Color;
        public Color Color
        {
            get { return _Color; }
            set
            {
                if (_Color != value)
                {
                    _Color = value;
                    OnPropertyChanged(nameof(Color));
                    TextColorChange();
                    OnPropertyChanged(nameof(TextColor));
                }
            }
        }
        public Color TextColor { get; set; }
        void TextColorChange()
        {
            TextColor = Color.FromArgb(
        255,
        (byte)(255 - _Color.R),
        (byte)(255 - _Color.G),
        (byte)(255 - _Color.B));
        }
        protected ProductBase(string name, int cost, Color color, int id = 0)
        {
            _Name = name;
            _Cost = cost;
            _Color = color;
            TextColorChange();
            if (id != 0)
                ID = id;
            else
            {
                Random r = new Random();
                ID = r.Next(10000,100000);
            }
        }

        public event PropertyChangedEventHandler? PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public override string ToString()
        {
            return $"{Name} {Cost}";
        }
        public override bool Equals(object? obj)
        {
            return obj is ProductBase @base &&
                   ID == @base.ID;  
        }
    }
}
