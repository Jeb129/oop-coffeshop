﻿using System.Windows.Media;

namespace DrinkMenu.Models
{
    public class Toping: ProductBase
    {
        int _MaxCount;

        public Toping(string name, int maxCount ,int cost, Color color,int id = 0) : base(name, cost, color, id)
        {
            _MaxCount = maxCount;
        }

        public int MaxCount
        {
            get { return _MaxCount; }
            set
            {
                if (_MaxCount != value)
                {
                    _MaxCount = value;
                    OnPropertyChanged(nameof(MaxCount));
                }
            }
        }
    }
}
