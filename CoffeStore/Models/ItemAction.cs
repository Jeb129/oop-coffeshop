﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace DrinkMenu.Models
{
    public static class ItemAction<T>
    {
        public static ObservableCollection<T> GetItems(string source)
        {
            string[] data = !File.Exists(source) ? new string[0] : File.ReadAllLines(source);
            ObservableCollection<T> items = new();
            foreach (string item in data)
                if(item != "")
                    items.Add(JsonSerializer.Deserialize<T>(item));
            return items;
        }
        public static void WriteItems(Collection<T> items, string tPath)
        {
            if (tPath == "") return;
            StreamWriter file = File.CreateText(tPath);
            foreach (T item in items)
                file.WriteLine(JsonSerializer.Serialize(item, typeof(T)));
            file.Close();
        }
        public static void ItemAdd(T item, Collection<T> items)
        {
            if(!items.Contains(item)) 
                items.Add(item);
        }
        public static T GetItem(T item, Collection<T> items)
        {
            if (items.Contains(item))
                return items[items.IndexOf(item)];
            return default;
        }
        public static T GetItem(int id, Collection<T> items)
        {
            foreach (T item in items)
                if((item as ProductBase).ID == id) return item;
            return default;
        }
    }
}
