﻿using DrinkMenu.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;

namespace CoffeStore.Views
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            DirectorySetup();
            InitializeComponent();
            firstmach.DrinkMenuVM.DrinkSrcPath = Environment.CurrentDirectory + "/Resources/Drinks.json";
            firstmach.DrinkMenuVM.TopingSrcPath = Environment.CurrentDirectory + "/Resources/Topings.json";
            secondmach.DrinkMenuVM.DrinkSrcPath = Environment.CurrentDirectory + "/Resources/SDrinks.json";
            secondmach.DrinkMenuVM.TopingSrcPath = Environment.CurrentDirectory + "/Resources/STopings.json";
        }
        private void DirectorySetup()
        {
            if (!Directory.Exists(Environment.CurrentDirectory + "/Resources"))
                Directory.CreateDirectory(Environment.CurrentDirectory + "/Resources");
            if (!File.Exists(Environment.CurrentDirectory + "/Resources/SDrinks.json"))
                File.Create(Environment.CurrentDirectory + "/Resources/SDrinks.json");
            if (!File.Exists(Environment.CurrentDirectory + "/Resources/STopings.json"))
                File.Create(Environment.CurrentDirectory + "/Resources/STopings.json");
            if (!File.Exists(Environment.CurrentDirectory + "/Resources/Drinks.json"))
                File.Create(Environment.CurrentDirectory + "/Resources/Drinks.json");
            if (!File.Exists(Environment.CurrentDirectory + "/Resources/Topings.json"))
                File.Create(Environment.CurrentDirectory + "/Resources/Topings.json");
        }
        private void Window_Closing(object sender, CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
