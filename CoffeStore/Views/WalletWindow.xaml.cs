﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CoffeStore.Views
{
    /// <summary>
    /// Логика взаимодействия для WalletWindow.xaml
    /// </summary>
    public partial class WalletWindow : Window
    {
        public WalletWindow()
        {
            InitializeComponent();
        }
        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Control control = sender as Control;
            DragDrop.DoDragDrop(control,control.Tag.ToString(), DragDropEffects.Move);
        }
        private void Money_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Image Img = (Image)sender;
            DragDrop.DoDragDrop(Img, Img.Tag.ToString(), DragDropEffects.Move);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Hide();
        }
    }
}
