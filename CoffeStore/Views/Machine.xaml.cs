﻿using DrinkMenu.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CoffeStore.Views
{
    /// <summary>
    /// Логика взаимодействия для Machine.xaml
    /// </summary>
    public partial class Machine : UserControl, INotifyPropertyChanged
    {
        public Machine()
        {
            DataContext = this;
            DrinkMenuVMInit();
            InitializeComponent();
        }
        #region VM меню
        WalletWindow wWindow;
        DrinkMenuVmodel _DrinkMenuVM = new DrinkMenuVmodel();
        public DrinkMenuVmodel DrinkMenuVM
        {
            get { return _DrinkMenuVM; }
            set { _DrinkMenuVM = value; }
        }
        public int Wallet
        {
            get { return _DrinkMenuVM.Wallet; }
            set
            {
                if (_DrinkMenuVM.Wallet != value)
                {
                    _DrinkMenuVM.Wallet = value;
                    OnPropertyChanged(nameof(Wallet));
                }
            }
        }
        public Color final;
        public void DrinkMenuVMInit()
        {
            DrinkMenuVM.OrderCreatePercent = 30;
            DrinkMenuVM.OnOrderCreate += _DrinkMenuVM_OnOrderCreate;
        }
        private async void _DrinkMenuVM_OnOrderCreate()
        {
            InterGrid.Visibility = Visibility.Visible;
            DrinkMenuVM.Done = false;
            await Task.Delay(200);
            OnPropertyChanged(nameof(Wallet));
            Animation();
            _drinkCaptured = false;
            _walletCaptured = Wallet == 0;
        }
        #endregion

        bool _drinkCaptured = true;
        bool _walletCaptured = true;
        private void NowM_Drop(object sender, DragEventArgs e)
        {
            string tstring = e.Data.GetData(typeof(string)).ToString();
            Wallet += Int32.Parse(tstring);
        }
        private async void Animation()
        {
            DrinkColor();
            Drink.Background = InterGrid.Background;
            Cup.Background = new SolidColorBrush(final);
            Jet.Background = new SolidColorBrush(final);//
            DoubleAnimation JetAnim = new DoubleAnimation();
            JetAnim.From = 0;
            JetAnim.SpeedRatio = 3.8;
            JetAnim.To = Jet.Height;
            JetAnim.Duration = TimeSpan.FromSeconds(4);
            Jet.BeginAnimation(HeightProperty, JetAnim);
            DoubleAnimation CupAnim = new DoubleAnimation();
            CupAnim.From = Drink.Height;
            CupAnim.To = Drink.MinHeight;
            CupAnim.BeginTime = TimeSpan.FromSeconds(1);
            CupAnim.Duration = TimeSpan.FromSeconds(5);
            Drink.BeginAnimation(HeightProperty, CupAnim);
            await Task.Delay(6000);
            Jet.Background = new SolidColorBrush(Colors.Transparent);
            DoubleAnimation JetRet = new DoubleAnimation();
            JetRet.From = 35;
            JetRet.To = 0;
            JetRet.Duration = TimeSpan.FromSeconds(1);
            Jet.BeginAnimation(HeightProperty, JetRet);
        }
        private void NowM_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (wWindow != null)
                wWindow.Activate();
            else
                (wWindow ??= new()).Closed += WalletDestroy;
            wWindow.Show();

        }
        private void WalletDestroy(object? sender, EventArgs e)
        {
            wWindow = null;
        }

        public event PropertyChangedEventHandler? PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        private void ToggleEditMode(object sender, RoutedEventArgs e)
        {
            DrinkMenuVM.EditMode = !DrinkMenuVM.EditMode;
        }
        private void Drink_Click(object sender, MouseButtonEventArgs e)
        {
            if (DrinkMenuVM.OrderProgress == 100)
            {
                Cup.Background = new SolidColorBrush(Colors.Transparent);
                Jet.BeginAnimation(HeightProperty, null);
                Drink.BeginAnimation(HeightProperty, null);
                DrinkMenuVM.Done = true;
                InterGrid.Visibility = Visibility.Hidden;
                _drinkCaptured = true;
                ServeEnd();
            }
        }
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Wallet > 0 && (DrinkMenuVM.OrderProgress >= 31 || DrinkMenuVM.OrderProgress == 0))
            {
                Wallet = 0;
                Coin.Visibility = Visibility.Visible;
                await Task.Delay(2500);
                Coin.Visibility = Visibility.Hidden;
                _walletCaptured = true;
                ServeEnd();
            }
        }
        private void DrinkColor()
        {
            Color a = DrinkMenuVM.SelectedDrink.Color;
            float R = a.ScR;
            float G = a.ScG;
            float B = a.ScB;
            foreach (var item in DrinkMenuVM.SelectedTopings)
                for (int i = 0; i < item.Count; i++)
                {
                    Color b = item.Toping.Color;
                    R += (((b.ScR - R) * item.Count) / 10);
                    G += (((b.ScG - G) * item.Count) / 10);
                    B += (((b.ScB - B) * item.Count) / 10);
                }
            final = Color.FromScRgb(1, R, G, B);
        }

        void ServeEnd()
        {
            if (_drinkCaptured && _walletCaptured
                && DrinkMenuVM.ProgressView)
            {
                DrinkMenuVM.DrinksView = true;
                DrinkMenuVM.ProgressView = false;
                DrinkMenuVM.SelectedDrink = null;
            }
        }
    }
}
