﻿using CoffeStore.Models;
using CoffeStore.Views;
using DrinkMenu.Models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics.Metrics;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace DrinkMenu.ViewModels
{
    public class DrinkMenuVmodel : INotifyPropertyChanged
    {
        #region Хранение и получение данных
        ObservableCollection<Toping> _Topings;
        public ObservableCollection<Toping> Topings
        {
            get 
            { 
                _Topings ??= new();
                var NewItem = (ListCollectionView)CollectionViewSource.GetDefaultView(_Topings);
                NewItem.NewItemPlaceholderPosition = EditMode ? NewItemPlaceholderPosition.AtBeginning : NewItemPlaceholderPosition.None;
                return _Topings;
            }
            set => _Topings = value;
        }
        ObservableCollection<Drink> _Drinks;
        public ObservableCollection<Drink> Drinks
        {
            get
            {
                _Drinks ??= new();
                var NewItem = (ListCollectionView)CollectionViewSource.GetDefaultView(_Drinks);
                NewItem.NewItemPlaceholderPosition = EditMode ? NewItemPlaceholderPosition.AtBeginning : NewItemPlaceholderPosition.None;
                return _Drinks;
            }
            set => _Drinks = value;
        }
        string _DrinkScrPath = string.Empty;
        public string DrinkSrcPath
        {
            get { return _DrinkScrPath; }
            set
            {
                if (_DrinkScrPath != value)
                {
                    _DrinkScrPath = value;
                    if (_DrinkScrPath != string.Empty)
                        if (File.Exists(_DrinkScrPath))
                            Drinks = ItemAction<Drink>.GetItems(DrinkSrcPath);
                }
            }
        }
        string _TopingScrPath = string.Empty;
        public string TopingSrcPath
        {
            get { return _TopingScrPath; }
            set
            {
                if (_TopingScrPath != value)
                {
                    _TopingScrPath = value;
                    if (_TopingScrPath != string.Empty)
                        if (File.Exists(_TopingScrPath))
                            Topings = ItemAction<Toping>.GetItems(TopingSrcPath);
                }
            }
        }
        int _Wallet;
        public int Wallet
        {
            get { return _Wallet; }
            set
            {
                if (_Wallet != value)
                {
                    _Wallet = value;
                    OnPropertyChanged(nameof(Wallet));
                }
            }
        }

        RelayCommand _ChangeAvatar;
        public RelayCommand ChangeAvatar
        {
            get
            {
                return _ChangeAvatar ??=
                    new RelayCommand(obj =>
                    {
                        AvaterPathSelect();
                    });
            }
        }
        public bool Done = true;
        void AvaterPathSelect()
        {
            OpenFileDialog file = new OpenFileDialog();
            file.ShowDialog();
            if (file.FileName != "" && file.FileName != SelectedDrink.AvatarPath)
                SelectedDrink.AvatarPath = file.FileName;
        }
        #endregion
        #region Настройки отображения
        bool _EditMode;
        public bool EditMode //Режим редактирования
        {
            get { return _EditMode; }
            set
            {
                if (_EditMode != value)
                {
                    _EditMode = value;
                    OnPropertyChanged(nameof(EditMode));
                    OnPropertyChanged(nameof(Drinks));
                    OnPropertyChanged(nameof(Topings));
                }
            }
        }

        bool _ConfirmView;
        public bool ConfirmView
        {
            get { return _ConfirmView; }
            set
            {
                if (_ConfirmView != value)
                {
                    _ConfirmView = value;
                    OnPropertyChanged(nameof(ConfirmView));
                }
            }
        }
        bool _DrinksView = true;
        public bool DrinksView
        {
            get { return _DrinksView; }
            set
            {
                if (_DrinksView != value)
                {
                    _DrinksView = value;
                    OnPropertyChanged(nameof(DrinksView));
                }
            }
        }
        bool _TopingsView;
        public bool TopingsView
        {
            get { return _TopingsView; }
            set
            {
                if (_TopingsView != value)
                {
                    _TopingsView = value;
                    OnPropertyChanged(nameof(TopingsView));
                    ItemAction<Toping>.WriteItems(Topings, TopingSrcPath);
                }
            }
        }
        bool _ProgressView;
        public bool ProgressView
        {
            get { return _ProgressView; }
            set
            {
                if (_ProgressView != value)
                {
                    _ProgressView = value;
                    OnPropertyChanged(nameof(ProgressView));
                }
            }
        }
        RelayCommand _ShowDrinks;
        public RelayCommand ShowDrinks
        {
            get
            {
                return _ShowDrinks ??= 
                  new RelayCommand(obj =>
                  {
                      DrinksView = true;
                      ProgressView = false;
                      SelectedDrink = null;
                  },obj =>SelectedDrink != null && SelectedDrink.Name != "");
            }
        }

        RelayCommand _ShowTopingsToggle;
        public RelayCommand ShowTopingsToggle
        {
            get
            {
                return _ShowTopingsToggle ??=
                  new RelayCommand(obj =>
                  {
                   TopingsView = !TopingsView;
                      if (!TopingsView)
                          ItemAction<Toping>.WriteItems(Topings, TopingSrcPath);
                  }, obj => SelectedDrink != null && SelectedDrink.Name != "");//canExecute
            }
        }
        public bool HasTopings
        {
            get { return DrinkTopings.Count > 0; }
        }
        async void ProgressTic(int prcnt)
        {
            OrderProgress = 0;
            OnPropertyChanged(nameof(DenyOrder));
            while (OrderProgress < 100)
            {
                OrderProgress++;

                if (OrderProgress == prcnt)
                {
                    OnOrderCreate();
                    Wallet -= Sum;
                }
                await Task.Delay(100);
            }
            OnPropertyChanged(nameof(DenyOrder));
        }
        #endregion
        #region Заказ
        Drink _SelectedDrink; 
        public Drink SelectedDrink
        {
            get { return _SelectedDrink; }
            set
            {
                if (_SelectedDrink != value)
                {
                    _SelectedDrink = value;
                    OnPropertyChanged(nameof(SelectedDrink));
                    if (_SelectedDrink != null)
                        DrinkSelecting();
                    else
                    {
                        SelectedTopings.Clear();
                        DrinkTopings.Clear();
                        ItemAction<Drink>.WriteItems(Drinks, DrinkSrcPath);
                    }
                }
            }
        } // Выбраный напиток
        ObservableCollection<Toping> _DrinkTopings; 
        public ObservableCollection<Toping> DrinkTopings
        {
            get => _DrinkTopings ??= new();
            set => _DrinkTopings = value;
        } // Доступные топинги
        ObservableCollection<TopingNode> _SelectedTopings; 
        public ObservableCollection<TopingNode> SelectedTopings
        {
            get => _SelectedTopings ??= new();
            set => _SelectedTopings = value;
        } // ВЫбраные топинги
        #region Команды топингов
        RelayCommand _InCount;
        public RelayCommand InCount
        {
            get
            {
                return _InCount ??=
                  new RelayCommand(obj =>
                  {
                      (obj as TopingNode).Count++;
                      Sum += (obj as TopingNode).Toping.Cost;
                  }, obj => obj != null && (obj as TopingNode).Count < (obj as TopingNode).Toping.MaxCount);//canExecute
            }
        }

        RelayCommand _DeCount;
        public RelayCommand DeCount
        {
            get
            {
                return _DeCount ??=
                  new RelayCommand(obj =>
                  {
                      (obj as TopingNode).Count--;
                      Sum -= (obj as TopingNode).Toping.Cost;
                      if ((obj as TopingNode).Count < 1)
                          SelectedTopings.Remove(obj as TopingNode);
                  }, obj => obj != null && (obj as TopingNode).Count > 0);//canExecute
            }
        }

        RelayCommand _AddNode;
        public RelayCommand AddNode
        {
            get
            {
                return _AddNode ??=
                  new RelayCommand(obj =>
                  {
                      SelectedTopings.Add(new(obj as Toping));
                      Sum += (obj as Toping).Cost;
                  }, obj =>
                  {
                      if (!(obj is Toping toping)) return false;
                      foreach (TopingNode node in SelectedTopings)
                          if (node.Toping == toping)
                              return false;
                      return true;
                  });
            }
        }

        RelayCommand _DelNode;
        public RelayCommand DelNode
        {
            get
            {
                return _DelNode ??=
                  new RelayCommand(obj =>
                  {
                      SelectedTopings.Remove(obj as TopingNode);
                  }, obj => true);
            }
        }
        #endregion
        int _Sum; // Сумма заказа
        public int Sum
        {
            get { return _Sum; }
            set
            {
                if (_Sum != value)
                {
                    _Sum = value;
                    OnPropertyChanged(nameof(Sum));
                }
            }
        }
        int _OrderProgress;
        public int OrderProgress
        {
            get { return _OrderProgress; }
            set
            {
                if (_OrderProgress != value)
                {
                    _OrderProgress = value;
                    OnPropertyChanged(nameof(OrderProgress));
                    OnPropertyChanged(nameof(oProgressMessage));
                }
            }
        }
        public string oProgressMessage
        {
            get
            {
                if (OrderProgress < OrderCreatePercent)
                    return $"Подготовка... {OrderProgress}%";
                if (OrderProgress == 100)
                    return "Заберите напиток\nи сдачу";
                return $"Приготовление... {OrderProgress}%";
            }
        }
        public delegate void OrderCreate();
        public event OrderCreate? OnOrderCreate;
        public int OrderCreatePercent { get; set; }

        RelayCommand _CreateOrder;
        public RelayCommand CreateOrder
        {
            get
            {
                return _CreateOrder ??=
                  new RelayCommand(obj =>
                  {
                          ProgressView = true;
                          ProgressTic(OrderCreatePercent);
                  }, obj => OnOrderCreate != null && Wallet >= Sum && Done);//canExecute
            }
        }

        RelayCommand _DenyOrder;
        public RelayCommand DenyOrder
        {
            get
            {
                return OrderProgress == 100? ShowDrinks : _DenyOrder ??=
                  new RelayCommand(obj =>
                  {
                      OrderProgress = 100;
                      ProgressView = false;
                  }, obj => OrderProgress < OrderCreatePercent || OrderProgress== 100);//canExecute
            }
        }
        void DrinkSelecting()
        {
            DrinksView = false;
            Sum = SelectedDrink.Cost;
            GetDrinkTopings();
        }
        void GetDrinkTopings()
        {
            DrinkTopings = new();
            foreach (int id in SelectedDrink.AllowedTopings)
                ItemAction<Toping>.ItemAdd(ItemAction<Toping>.GetItem(id,Topings),DrinkTopings);
            var topingView = (ListCollectionView)CollectionViewSource.GetDefaultView(DrinkTopings);
            topingView.NewItemPlaceholderPosition = EditMode ? NewItemPlaceholderPosition.AtBeginning : NewItemPlaceholderPosition.None;
            OnPropertyChanged(nameof(DrinkTopings));
            OnPropertyChanged(nameof(HasTopings));
        }
        #endregion
        #region Редактирование
        RelayCommand _AddDrink;
        public RelayCommand AddDrink
        {
            get
            {
                return _AddDrink ??=
                  new RelayCommand(obj =>
                  {
                      SelectedDrink = new("", 0, Colors.White, "");
                      Drinks.Add(SelectedDrink);
                  }, obj => EditMode);//canExecute
            }
        }
        RelayCommand _RemoveDrink;
        public RelayCommand RemoveDrink
        {
            get
            {
                return !Removing ? ConfirmRemove : _RemoveDrink ??=
                  new RelayCommand(obj =>
                  {
                      Drinks.Remove(SelectedDrink);
                      DrinksView = true;
                      Removing = false;
                  }, obj => true);//canExecute
            }
        }
        RelayCommand _AddToping;
        public RelayCommand AddToping
        {
            get
            {
                return _AddToping ??=
                  new RelayCommand(obj =>
                  {
                      Topings.Add(new("", 0, 0,Colors.White));
                  }, obj => EditMode);//canExecute
            }
        }
        RelayCommand _RemoveToping;
        public RelayCommand RemoveToping
        {
            get
            {
                return !Removing ? ConfirmRemove : _RemoveToping ??=
                  new RelayCommand(obj =>
                  {
                      Topings.Remove(obj as Toping);
                      Removing = false;
                      foreach (Drink drink in Drinks)
                          drink.AllowedTopings.Remove((obj as Toping).ID);
                  });
            }
        }
        RelayCommand _AddDrinkToping;
        public RelayCommand AddDrinkToping
        {
            get
            {
                return _AddDrinkToping ??=
                  new RelayCommand(obj =>
                  {
                      SelectedDrink.AllowedTopings.Add((obj as Toping).ID);
                      GetDrinkTopings();
                      TopingsView = false;
                  }, obj => obj != null && (obj as Toping).Name != "" && !DrinkTopings.Contains(obj as Toping));//canExecute
            }
        }
        RelayCommand _RemoveDrinkToping;
        public RelayCommand RemoveDrinkToping
        {
            get
            {
                return _RemoveDrinkToping ??=
                  new RelayCommand(obj =>
                  {
                      SelectedDrink.AllowedTopings.Remove((int)obj);
                      GetDrinkTopings();
                      Removing = false;
                  });//canExecute
            }
        }

        bool _Removing;
        bool Removing
        {
            get { return _Removing; }
            set
            {
                if (_Removing != value)
                {
                    _Removing = value;
                    OnPropertyChanged(nameof(Removing));
                    OnPropertyChanged(nameof(RemoveDrink));
                    OnPropertyChanged(nameof(RemoveToping));
                    OnPropertyChanged(nameof(RemoveDrinkToping));
                }
            }
        }

        RelayCommand _ConfirmRemove;
        public RelayCommand ConfirmRemove
        {
            get
            {
                return _ConfirmRemove ??=
                  new RelayCommand(obj =>
                  {
                      ConfirmView = true;
                  }, obj => true);//canExecute
            }
        }

        RelayCommand _RemoveAnsw;
        public RelayCommand RemoveAnsw
        {
            get
            {
                return _RemoveAnsw ??=
                  new RelayCommand(obj =>
                  {
                      Removing = bool.Parse((string)obj);
                      ConfirmView = false;
                  }, obj => true);//canExecute
            }
        }
        #endregion
        public event PropertyChangedEventHandler? PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
